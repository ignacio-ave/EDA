
from KnightsTour import InstanciaGrafo, EstadoGrafo, AmbienteGrafo
from KnightsTour import evalWarnsdorfActions, evalConstructiveActions
import numpy as np
from classic_algorithms import *
import sys
import time

from copy import deepcopy
import random


class KnightTourGreedy:
    def __init__(self, knight_problem):
        self.problem = knight_problem

    def search(self):
        current_state = self.problem.iniciar_estado()
        while not current_state.es_completo:
            moves = evalWarnsdorfActions(current_state, self.problem.env)
            if not moves:
                break
        
            next_move = min(moves, key=lambda x: len(self.problem.env.generar_acciones(EstadoGrafo(self.problem.instancia_grafo, x))))
            current_state.actualizar_estado(next_move)
            
            
        return current_state.visitados if current_state.es_completo else None
    

class KnightTourDFS:
    def __init__(self, environment, start_position):
        self.environment = environment
        self.start_position = start_position
        self.N = environment.instancia_grafo.N

    def search(self):
        stack = [(self.start_position, [self.start_position])]
        visited = set()

        while stack:
            current_position, path = stack.pop()
            if len(path) == self.N ** 2:
                return path

            for move in self.environment.instancia_grafo.obtener_movimientos_legales(current_position):
                if move not in path:
                    new_path = path + [move]
                    stack.append((move, new_path))

        return None


class KnightTourBFS:
    def __init__(self, environment, start_position):
        self.environment = environment
        self.start_position = start_position
        self.N = environment.instancia_grafo.N

    def search(self):
        queue = [(self.start_position, [self.start_position])]
        visited = set()

        while queue:
            current_position, path = queue.pop(0)
            if len(path) == self.N ** 2:
                return path

            for move in self.environment.instancia_grafo.obtener_movimientos_legales(current_position):
                if move not in path:
                    new_path = path + [move]
                    queue.append((move, new_path))

        return None
"""

def main():
    N = 8
    start_position = (random.randint(0, N-1), random.randint(0, N-1))
    graph_instance = InstanciaGrafo(N)
    environment = AmbienteGrafo(graph_instance)
    
    # Para DFS
    dfs_agent = KnightTourDFS(environment, start_position)
    dfs_solution = dfs_agent.search()
    if dfs_solution:
        print("DFS Knight's Tour found:")
        print(dfs_solution)
    else:
        print("No complete DFS tour was found.")
    
    # Para BFS
    bfs_agent = KnightTourBFS(environment, start_position)
    bfs_solution = bfs_agent.search()
    if bfs_solution:
        print("BFS Knight's Tour found:")
        print(bfs_solution)
    else:
        print("No complete BFS tour was found.")

if __name__ == "__main__":
    main()

"""




class AgenteWarnsdorfBusquedaLocal:
    def __init__(self, N, posicion_inicial):
        self.grafo = InstanciaGrafo(N)
        self.estado = EstadoGrafo(self.grafo, posicion_inicial)
        self.ambiente = AmbienteGrafo(self.grafo)
        self.intentos_fallidos = set()  # Inicializamos el conjunto de intentos fallidos
        self.pasos = 0  # Para contar los pasos dados

        self.k = 5  # Coeficiente base de complejidad por casilla
        self.f = 10  # Factor de incremento por fallos consecutivos

        self.historial = []
        self.iteracion_costo = []

    def calcular_max_intentos(self):
        return self.k * (self.grafo.N ** 2) + self.f * self.estado.fallos_consecutivos

    def registrar_solucion(self, es_exitoso, solucion=None):
        # Función para registrar el intento de solución
        self.historial.append({
            'bloque': self.grafo.N,  # Añadimos el tamaño del tablero como 'bloque'
            'tablero': f'{self.grafo.N}x{self.grafo.N}',
            'posicion_inicial': self.estado.posicion_actual,
            'es_exitoso': es_exitoso,
            'solucion': solucion if es_exitoso else 'Sin solución'
        })

    def imprimir_tablero(self):
        tablero = np.zeros((self.grafo.N, self.grafo.N))
        for pos in self.estado.visitados:
            tablero[pos] = 1
        print(tablero)

    def aplicar_heuristica_warnsdorf(self, movimientos):
        # print(f"Aplicando heurística de Warnsdorf desde {self.estado.posicion_actual}. Movimientos posibles: {movimientos}")

        mejor_movimiento = None
        min_movimientos_siguientes = float('inf')
        for movimiento in movimientos:
            if movimiento in self.estado.visitados:
                continue  # Saltar movimientos ya visitados
            siguientes = self.grafo.obtener_movimientos_legales(movimiento)
            num_siguientes = sum(s not in self.estado.visitados for s in siguientes)
            if num_siguientes < min_movimientos_siguientes:
                min_movimientos_siguientes = num_siguientes
                mejor_movimiento = movimiento

        if mejor_movimiento is not None:
            # print(f"Warnsdorf moviendo a {mejor_movimiento} desde {self.estado.posicion_actual}")
            self.estado.actualizar_estado(mejor_movimiento)
            return True
        else:
            # print(f"No hay movimientos Warnsdorf posibles desde {self.estado.posicion_actual}")
            return False

    def aplicar_busqueda_local(self):
        # print("Intentando búsqueda local...")
        # Mejora: Permitir un retroceso más profundo si es necesario
        indice_actual = len(self.estado.visitados) - 1

        # Intentar retroceder hasta encontrar un movimiento alternativo viable
        while indice_actual > 0:
            self.estado.posicion_actual = self.estado.visitados[indice_actual]
            movimientos_posibles = self.ambiente.generar_acciones(self.estado)
            movimientos_no_intentados = [m for m in movimientos_posibles if (self.estado.posicion_actual, m) not in self.intentos_fallidos]

            # Si hay movimientos no intentados desde esta posición, intentar el próximo movimiento posible
            if movimientos_no_intentados:
                movimiento_siguiente = movimientos_no_intentados[0]  # Tomar el primer movimiento no intentado
                self.estado.actualizar_estado(movimiento_siguiente)
                # Limpiar el historial de visitados para reflejar el nuevo camino tomado
                self.estado.visitados = self.estado.visitados[:indice_actual + 1]
                return True  # Indica que se ha encontrado un camino alternativo

            # No hay movimientos no intentados desde esta posición, retroceder otro paso
            indice_actual -= 1

        # Si se ha retrocedido al inicio y no se encuentran movimientos viables, no hay solución desde esta ruta
        return False

    def iniciar_busqueda(self):
        #print(f"Iniciando búsqueda para tablero de {self.grafo.N}x{self.grafo.N} desde {self.estado.posicion_actual}")
        while not self.estado.es_completo:
            MAX_INTENTOS = self.calcular_max_intentos()
            
            self.iteracion_costo.append((self.pasos, self.estado.nuevo_costo()))  # Guardamos el costo actual
            
            if self.pasos >= MAX_INTENTOS:
                # print("Se alcanzó el número máximo de intentos sin encontrar una solución.")
                return None, self.estado.fallos_consecutivos, self.iteracion_costo


            # print(f"Visitados: {len(self.estado.visitados)}")
            self.pasos += 1
            movimientos = self.ambiente.generar_acciones(self.estado)

            if not movimientos:
                # print("No hay movimientos disponibles, intentando búsqueda local...")
                if not self.aplicar_busqueda_local():
                    #print("Búsqueda local falló, no se puede continuar.")
                    return None

            elif not self.aplicar_heuristica_warnsdorf(movimientos):
                # print("Warnsdorf falló, intentando búsqueda local...")
                if not self.aplicar_busqueda_local():
                    # print("Búsqueda local falló después de Warnsdorf, no se puede continuar.")
                    return None

            #print(f"Paso {self.pasos}: Moviendo a {self.estado.posicion_actual}")
            #self.imprimir_tablero()
            
        if self.estado.es_completo:
            # print("Tour completo encontrado:")
            return self.estado.visitados, self.estado.fallos_consecutivos, self.iteracion_costo
        else:
            return None, self.estado.fallos_consecutivos, self.iteracion_costo



class Perturbation:
    def __init__(self, ambiente, pert_size=3):
        self.ambiente = ambiente
        self.pert_size = pert_size

    def __call__(self, state):
        current_state = deepcopy(state)
        for _ in range(self.pert_size):
            moves = self.ambiente.generar_acciones(current_state, tipo="warnsdorf")
            if moves:
                move = random.choice(moves)
                current_state.actualizar_estado(move)
        return current_state

class ILS:
    def __init__(self, local_search, perturbation, max_iter=50):
        self.local_search = local_search
        self.perturbation = perturbation
        self.max_iter = max_iter
        self.iteracion_costo = []
        
    def __call__(self, initial_solution):
        current_solution = deepcopy(initial_solution)
        best_solution = deepcopy(current_solution)
        best_cost = len(best_solution.visitados)
        self.iteracion_costo = []

        for iter_num in range(self.max_iter):
            
            perturbed_solution = self.perturbation(current_solution)
            local_optimum = self.local_search(perturbed_solution)
            local_cost = len(local_optimum.visitados)
            
            if local_cost > best_cost:
                best_solution = deepcopy(local_optimum)
                best_cost = local_cost

            self.iteracion_costo.append((iter_num, local_optimum.nuevo_costo()))
             
        return best_solution, self.iteracion_costo 
    
    

def local_search_method(state, ambiente):
    current_state = deepcopy(state)
    while True:
        moves = ambiente.generar_acciones(current_state, tipo="warnsdorf")
        if not moves:
            break
        best_move = moves[0]  # La heurística de Warnsdorf ya ordena los movimientos
        current_state.actualizar_estado(best_move)
    return current_state







class TabuSearch:
    def __init__(self, problem, max_iter=100, tabu_size=15, aspiration=True):
        self.problem = problem
        self.max_iter = max_iter
        self.tabu_size = tabu_size
        self.tabu_list = []
        self.aspiration = aspiration


    def search(self):
        current_state = self.problem.iniciar_estado()
        best_cost = float('inf')

        for iteration in range(self.max_iter):
            moves = evalConstructiveActions(current_state, self.problem.env)
            moves.sort(key=lambda x: x[1])

            found_better = False
            for move, score in moves:
                if move not in self.tabu_list or (self.aspiration and score < best_cost):
                    new_state = deepcopy(current_state)
                    new_state.actualizar_estado(move)
                    new_cost = self.problem.costo(new_state)
                    if new_cost < best_cost:
                        best_state = new_state
                        best_cost = new_cost
                        self.tabu_list.append(move)
                        if len(self.tabu_list) > self.tabu_size:
                            self.tabu_list.pop(0)
                        found_better = True
                        break
            if not found_better:
                break

        return best_state
    
    
class TabuSearchAgent:
    def __init__(self, environment, start_position, tabu_size=10, max_iterations=1000):
        self.environment = environment
        self.current_position = start_position
        self.tabu_list = []
        self.tabu_size = tabu_size
        self.max_iterations = max_iterations
        self.estado = EstadoGrafo(environment.instancia_grafo, start_position)
        self.iteracion_costo = []


    def search(self):
        self.iteracion_costo = []
        for iter_num in range(self.max_iterations):
            if self.estado.es_completo:
                # Agrega la última iteración y costo antes de retornar
                self.iteracion_costo.append((iter_num, self.estado.nuevo_costo()))
                # Asegúrate de retornar ambos valores aquí
                return self.estado.visitados, self.iteracion_costo

            possible_actions = self.environment.generar_acciones(self.estado)
            if not possible_actions:
                break
            best_action = self.select_next_position(possible_actions)
            self.estado.actualizar_estado(best_action)
            # Guardamos la iteración y el nuevo costo
            self.iteracion_costo.append((iter_num, self.estado.nuevo_costo()))
                        
        # También retorna ambos valores aquí para mantener la consistencia
        return self.estado.visitados if self.estado.es_completo else None, self.iteracion_costo
           
        # También retorna ambos valores aquí para mantener la consistencia
        return self.estado.visitados
    def select_next_position(self, neighbors):
        best_position = None
        min_unvisited_neighbors = float('inf')
        for position in neighbors:
            if position not in self.estado.visitados:
                possible_moves = self.environment.instancia_grafo.obtener_movimientos_legales(position)
                unvisited_neighbors_count = len([move for move in possible_moves if move not in self.estado.visitados])
                if unvisited_neighbors_count < min_unvisited_neighbors:
                    min_unvisited_neighbors = unvisited_neighbors_count
                    best_position = position
        return best_position
    
    
    

############################################################################################################

class KnightTourDFS:
    def __init__(self, environment, start_position):
        self.environment = environment
        self.start_position = start_position
        self.N = environment.instancia_grafo.N

    def search(self):
        stack = [(self.start_position, [self.start_position])]
        visited = set()

        while stack:
            current_position, path = stack.pop()
            if len(path) == self.N ** 2:
                return path

            for move in self.environment.instancia_grafo.obtener_movimientos_legales(current_position):
                if move not in path:
                    new_path = path + [move]
                    stack.append((move, new_path))

        return None


class KnightTourBFS:
    def __init__(self, environment, start_position):
        self.environment = environment
        self.start_position = start_position
        self.N = environment.instancia_grafo.N

    def search(self):
        queue = [(self.start_position, [self.start_position])]
        visited = set()

        while queue:
            current_position, path = queue.pop(0)
            if len(path) == self.N ** 2:
                return path

            for move in self.environment.instancia_grafo.obtener_movimientos_legales(current_position):
                if move not in path:
                    new_path = path + [move]
                    queue.append((move, new_path))

        return None


############################################################################################################

class HillClimbing:
    def __init__(self, generar_vecinos, costo):
        self.generar_vecinos = generar_vecinos
        self.costo = costo

    def __call__(self, initial_solution):
        current_solution = initial_solution
        current_cost = self.costo(current_solution)

        while True:
            neighbors = self.generar_vecinos(current_solution)
            if not neighbors:
                break  # No hay vecinos, por lo que no podemos hacer ningún movimiento

            # Encuentra el mejor vecino (el que tiene el menor costo)
            best_neighbor = min(neighbors, key=self.costo)
            best_neighbor_cost = self.costo(best_neighbor)

            # Si el mejor vecino no es mejor que la solución actual, hemos alcanzado un óptimo local
            if best_neighbor_cost >= current_cost:
                break

            # De lo contrario, mueve a la mejor vecina y continúa
            current_solution = best_neighbor
            current_cost = best_neighbor_cost

        return current_solution

    
    
class LocalSearchAgent: 
    def __init__ (self, N, posicion_inicial, iteraciones_sin_mejora=500):
        self.N=N
        self.posicion_inicial=posicion_inicial
        self.grafo = InstanciaGrafo(N)
        self.estado = EstadoGrafo(self.grafo, posicion_inicial)
        self.ambiente = AmbienteGrafo(self.grafo)
        self.iteraciones_sin_mejora = iteraciones_sin_mejora
        
    def hill_climbing(self):
        estado_actual = self.estado
        steps_without_improvement = 0
        max_steps_without_improvement = self.iteraciones_sin_mejora
        
        while not estado_actual.es_completo and steps_without_improvement < max_steps_without_improvement:
            neighbors = self.ambiente.generar_acciones(estado_actual,tipo="warnsdorf" )
            if not neighbors: 
                break
            next_move = min(neighbors, key=lambda x: len(self.ambiente.generar_acciones(EstadoGrafo(self.grafo, x))))
            if estado_actual.actualizar_estado(next_move): 
                steps_without_improvement=0
            else:
                steps_without_improvement+=1
                
        return estado_actual.visitados if estado_actual.es_completo else None 
    def solve(self):
        initial_solution = self.hill_climbing()
        if initial_solution:
            print("Solucion encontrada")

        else: 
            solucion_mejorada = self.hill_climbing()
            return solucion_mejorada
        
        
