import os
import json
import matplotlib.pyplot as plt

def plot_and_save_results(results_folder):
    # Check if the results folder exists
    if not os.path.exists(results_folder):
        print("Results folder does not exist.")
        return

    # Create a folder for plots if it doesn't exist
    plots_folder = os.path.join(results_folder, "plots")
    os.makedirs(plots_folder, exist_ok=True)

    # Read all results files and aggregate data
    aggregated_data = {}
    for filename in os.listdir(results_folder):
        if filename.endswith('.json'):
            file_path = os.path.join(results_folder, filename)
            with open(file_path, 'r') as file:
                data = json.load(file)
                for result in data:
                    N = result[0]['N']
                    for entry in result:
                        algo = entry['algorithm']
                        if algo not in aggregated_data:
                            aggregated_data[algo] = {}
                        if N not in aggregated_data[algo]:
                            aggregated_data[algo][N] = {'time_taken': [], 'steps_taken': []}
                        aggregated_data[algo][N]['time_taken'].append(entry['time_taken'])
                        aggregated_data[algo][N]['steps_taken'].append(entry['steps_taken'])

    # Plot for each algorithm
    for algo, data_by_N in aggregated_data.items():
        plt.figure(figsize=(10, 8))
        for N, data in data_by_N.items():
            plt.scatter(data['time_taken'], data['steps_taken'], label=f'N={N}')
        
        plt.xlabel('Time Taken (seconds)')
        plt.ylabel('Steps Taken')
        plt.title(f'Performance of {algo} Algorithm')
        plt.legend()
        plt.grid(True)
        
        # Save the plot
        plot_file_path = os.path.join(plots_folder, f'plot_{algo}.png')
        plt.savefig(plot_file_path)
        plt.close()



if __name__ == "__main__":
    results_folder = "results"
    plot_and_save_results(results_folder)
