# Knights Tour : Ignacio Astorga

## Declaración de Ambiente + Instancia + Estado (Environment + Instance + State)
- [KnightsTour.py (GitLab)](https://gitlab.com/ignacio-ave/EDA/-/blob/main/KnightsTour.py?ref_type=heads)
- [Notebook en Google Colab](https://colab.research.google.com/drive/1BqoR1iLbEDEZSzbARfE0QYQcB4QzNq35?usp=sharing)

## ALGORITMOS IMPLEMENTADOS:
- **Greedy Warnsdorf**
- **ILS**
- **Tabu**
- **TabuKeras** (Modelo Keras entrenado para predecir el siguiente movimiento viendo la instancia o estado actual del problema con lista tabú)

## Algoritmos definidos entre GitLab y Colab
- **Fundamentales:** [Aproximaciones principalmente teóricas (GitLab)](https://gitlab.com/ignacio-ave/EDA/-/blob/main/classic_algorithms.py?ref_type=heads)
- **Agentes:** [Agentes creados inspirados en las aproximaciones (GitLab)](https://gitlab.com/ignacio-ave/EDA/-/blob/main/agents.py?ref_type=heads)
- **TabuKeras:** [Creación de datos y entrenamiento de modelo + Agente con modelo + Plots (Colab)](https://colab.research.google.com/drive/1kc1_asrBgI3TKioNBCa7k_0l45-t_icj#scrollTo=CUNSqZeYYI4S)

## Detalles de Implementación
- **test runs**: Número de iteraciones a realizar por \(N\).
- **N**: Dimensión de la tabla de ajedrez.
- **range(5,100)**: Se testeará desde la tabla 5x5 hasta 100x100.
- **model_file_path**: Ruta al modelo ya entrenado, en este caso es entrenado con 1000 datos.
- Se comparará con los algoritmos *Tabu*, *TabuKeras* y *Warnsdorf*.

- **tabukeras** usa el modelo como variable global.

- Los algoritmos *ILS*, *Tabu*, *TabuKeras* y *Warnsdorf* (Greedy) pueden ser llamados ya que los agentes están implementados, pero en las aproximaciones teóricas se pueden encontrar otros algoritmos que nos competen.

```python
test_runs = 3
results = []
for N in range(5, 100):
    model_file_path ='/content/EDA/Knight-1000.h5'
    model = load_model_from_file(model_file_path)
    algorithms = ['Tabu', 'TabuKeras', 'Warnsdorf']
    result = test_knights_tour(N, algorithms, test_runs)
    results.append(result)
    save_results(results)
print(results)
```

Output esperado: 
```plaintext
Algorithm: Tabu, Start Position: (0, 3), Tour Found: True, Steps: 25, Time: 0.00035643577575683594
Algorithm: Tabu, Start Position: (0, 1), Tour Found: True, Steps: 25, Time: 0.00016069412231445312
Algorithm: Tabu, Start Position: (3, 3), Tour Found: True, Steps: 25, Time: 0.00015926361083984375
Algorithm: TabuKeras, Start Position: (2, 4), Tour Found: True, Steps: 25, Time: 0.00018143653869628906
Algorithm: TabuKeras, Start Position: (2, 1), Tour Found: True, Steps: 25, Time: 0.00029468536376953125
Algorithm: TabuKeras, Start Position: (2, 3), Tour Found: True, Steps: 25, Time: 0.0002582073211669922
Algorithm: Warnsdorf, Start Position: (1, 2), Tour Found: False, Steps: -1, Time: 0.0006213188171386719
Algorithm: Warnsdorf, Start Position: (4, 1), Tour Found: False, Steps: -1, Time: 0.0006821155548095703
Algorithm: Warnsdorf, Start Position: (2, 3), Tour Found: False, Steps: -1, Time: 0.0005917549133300781
...
Algorithm: Tabu, Start Position: (36, 55), Tour Found: False, Steps: -1, Time: 0.22196674346923828
Algorithm: Tabu, Start Position: (83, 89), Tour Found: False, Steps: -1, Time: 0.21504855155944824
Algorithm: Tabu, Start Position: (74, 20), Tour Found: False, Steps: -1, Time: 0.22286534309387207
Algorithm: TabuKeras, Start Position: (72, 59), Tour Found: False, Steps: -1, Time: 0.2124178409576416
Algorithm: TabuKeras, Start Position: (13, 51), Tour Found: False, Steps: -1, Time: 0.21959948539733887
Algorithm: TabuKeras, Start Position: (46, 59), Tour Found: False, Steps: -1, Time: 0.2169206142425537
Algorithm: Warnsdorf, Start Position: (46, 27), Tour Found: False, Steps: -1, Time: 77.24910569190979
Algorithm: Warnsdorf, Start Position: (60, 4), Tour Found: True, Steps: 9604, Time: 33.47039866447449
Algorithm: Warnsdorf, Start Position: (73, 7), Tour Found: False, Steps: -1, Time: 100.67429184913635

```

Al parecer, Tabu tiene algún límite de iteraciones y, por eso, los tiempos de ejecución posteriores a los tableros 40x40 son los mismos. Esto explica por qué no encuentra rutas en tableros grandes.