from KnightsTour import InstanciaGrafo, EstadoGrafo, AmbienteGrafo
from KnightsTour import evalWarnsdorfActions, evalConstructiveActions, local_search_method, plot_knights_tour
from classic_algorithms import *


from agents import AgenteWarnsdorfBusquedaLocal, Perturbation, ILS ,local_search_method, TabuSearchAgent, KnightTourDFS, KnightTourBFS


import random
import time
import json
import os

def run_algorithm(N, algorithm_name, start_position=None):
    if start_position is None:
        start_position = (random.randint(0, N-1), random.randint(0, N-1))

    graph_instance = InstanciaGrafo(N)
    estado_inicial = EstadoGrafo(graph_instance, start_position)
    environment = AmbienteGrafo(graph_instance)
    start_time = time.time()

    if algorithm_name == 'ILS':
        agent = ILS(lambda s: local_search_method(s, environment), Perturbation(environment))
        solution, iteracion_costo = agent(estado_inicial)
        tour_found = solution is not None
        steps_taken = len(solution.visitados) if tour_found else -1
        final_solution = solution.visitados if tour_found else None
    elif algorithm_name == 'Tabu':
        agent = TabuSearchAgent(environment, start_position)
        solution, iteracion_costo = agent.search()
        tour_found = solution is not None
        steps_taken = len(solution) if tour_found else -1
        final_solution = solution if tour_found else None
    elif algorithm_name == 'Warnsdorf':
        agent = AgenteWarnsdorfBusquedaLocal(N, start_position)
        solution, fallos_consecutivos, iteracion_costo = agent.iniciar_busqueda()
        tour_found = (solution is not None) and (len(solution) == N*N)
        steps_taken = len(solution) if tour_found else -1
        final_solution = solution if tour_found else None
    else:
        raise ValueError(f"Algorithm {algorithm_name} not recognized")

    time_taken = time.time() - start_time
    result = {
        'algorithm': algorithm_name,
        'start_position': start_position,
        'tour_found': tour_found,
        'steps_taken': steps_taken,
        'time_taken': time_taken,
        'solution': final_solution,
        'N': N,
        'iteracion_costo': iteracion_costo  # Agrega la lista de iteración y costo al resultado
    }
    return result


def test_knights_tour(N, algorithms, test_runs):
    all_results = []

    for algorithm_name in algorithms:
        for _ in range(test_runs):
            result = run_algorithm(N, algorithm_name)
            all_results.append(result)
            print(f"Algorithm: {result['algorithm']}, Start Position: {result['start_position']}, Tour Found: {result['tour_found']}, Steps: {result['steps_taken']}, Time: {result['time_taken']}")
            # Optionally plot the tour using `plot_knights_tour` if a solution is found

    return all_results


def save_results(results):
    folder_path = "results"
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    for i, result in enumerate(results):
        file_path = os.path.join(folder_path, f"result_{i}.json")
        with open(file_path, 'w') as file:
            json.dump(result, file)



if __name__ == "__main__":
    test_runs = 2
    results = []
    for N in range(5, 8):
        algorithms = ['ILS', 'Tabu', 'Warnsdorf']
        result = test_knights_tour(N, algorithms, test_runs)
        results.append(result)
        print(result)
    save_results(results)





"""
# Example of how to use the test function
if __name__ == "__main__":
    N = 8
    algorithms = ['ILS', 'Tabu', 'Warnsdorf']
    test_runs = 10
    results = test_knights_tour(N, algorithms, test_runs)
    print(results)
    
    
def main():
    N = 8
    start_position = (random.randint(0, N-1), random.randint(0, N-1))
    print(f"Starting Knight's Tour from position: {start_position} on a {N}x{N} chessboard.")
    graph_instance = InstanciaGrafo(N)
    estado_inicial = EstadoGrafo(graph_instance, start_position)
    environment = AmbienteGrafo(graph_instance)  # Asegúrate que aquí se pase graph_instance
    agent = ILS(lambda s: local_search_method(s, environment), Perturbation(environment))
    solution = agent(estado_inicial)
    if solution:
        print("Knight's Tour found:")
        print(solution.visitados)
        plot_knights_tour(solution.visitados, N)
    else:
        print("No complete tour was found.")
        
if __name__ == "__main__":
    main()
    
        
def main():
    N = 8  # Asegúrate de definir el tamaño del tablero
    start_position = (random.randint(0, N-1), random.randint(0, N-1))
    print(f"Starting Knight's Tour from position: {start_position} on a {N}x{N} chessboard.")
    agent = AgenteWarnsdorfBusquedaLocal(N, start_position)  # Pasa N y start_position correctamente
    solution = agent.iniciar_busqueda()
    if solution:
        print("Knight's Tour found:")
        print(solution)
    else:
        print("No complete tour was found.")

        
        
if __name__ == "__main__":
    main()    

def main():
    N = 8
    start_position = (random.randint(0, N-1), random.randint(0, N-1))
    print(f"Starting Knight's Tour from position: {start_position} on a {N}x{N} chessboard.")
    graph_instance = InstanciaGrafo(N)
    environment = AmbienteGrafo(graph_instance)  # Asegúrate que aquí se pase graph_instance
    agent = TabuSearchAgent(environment, start_position)
    solution = agent.search()
    if solution:
        print("Knight's Tour found:")
        print(solution)
        plot_knights_tour(solution, N)
    else:
        print("No complete tour was found.")

if __name__ == "__main__":
    main()




"""