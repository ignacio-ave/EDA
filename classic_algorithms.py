
from KnightsTour import evalConstructiveActions
import random
from copy import deepcopy
import heapq
import logging

# Configuración básica del logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

import networkx as nx
import numpy as np

from copy import deepcopy
from collections import deque 



class Greedy:
    def __init__(self, evalConstructiveMoves, random=False, elite_factor=1.0):
        self.evalConstructiveMoves = evalConstructiveMoves
        self.random = random
        self.elite_factor = elite_factor

    def __call__(self, initial_state):
        current_state = initial_state
        while not current_state.isCompleteSolution():
            scored_moves = self.evalConstructiveMoves(current_state)
            if self.random:
                total_score = sum(score for _, score in scored_moves)
                normalized_scores = [(move, (score / total_score)**self.elite_factor) for move, score in scored_moves]
                best_move = random.choices([move for move, _ in normalized_scores], weights=[score for _, score in normalized_scores], k=1)[0]
            else:
                best_move, _ = max(scored_moves, key=lambda x: x[1])
            current_state.intentar_movimiento(best_move)
        return current_state


class DepthFirstSearch:
    def __init__(self, problem):
        self.problem = problem
        self.visited = set()

    def search(self):
        stack = [self.problem.iniciar_estado()]
        while stack:
            current_state = stack.pop()

            state_signature = self.problem.state_signature(current_state) if hasattr(self.problem, 'state_signature') else id(current_state)

            if current_state.isCompleteSolution():
                return current_state

            if state_signature in self.visited:
                continue
            self.visited.add(state_signature)

            for move in self.problem.get_legal_moves(current_state):
                new_state = deepcopy(current_state)
                success = new_state.intentar_movimiento(move)
                if success: 
                    stack.append(new_state)

        return None  




class BreadthFirstSearch:
    def __init__(self, problem):
        self.problem = problem

    def search(self):
        queue = deque([self.problem.iniciar_estado()])
        visited = set()  

        while queue:
            current_state = queue.popleft()

            state_signature = self.problem.state_signature(current_state)
            if state_signature in visited:
                continue
            visited.add(state_signature)

            if current_state.isCompleteSolution():
                return current_state 

            for move in self.problem.gen_vecinos(current_state):
                new_state = deepcopy(current_state)
                if new_state.intentar_movimiento(move):
                    queue.append(new_state)

        return None  


class TabuSearch:
    def __init__(self, problem, max_iter=100, tabu_size=15, aspiration=True):
        self.problem = problem
        self.max_iter = max_iter
        self.tabu_size = tabu_size
        self.tabu_list = []
        self.aspiration = aspiration
        

    def search(self):
        current_state = self.problem.iniciar_estado()
        best_cost = float('inf')

        for iteration in range(self.max_iter):
            logging.debug(f"Iteración {iteration}: posición actual {current_state.posicion_actual}")
            moves = evalConstructiveActions(current_state, self.problem.env)
            moves.sort(key=lambda x: x[1])

            found_better = False
            for move, score in moves:
                if move not in self.tabu_list or (self.aspiration and score < best_cost):
                    logging.debug(f"Aplicando movimiento: {move}")
                    new_state = deepcopy(current_state)
                    new_state.actualizar_estado(move)
                    new_cost = self.problem.costo(new_state)
                    if new_cost < best_cost:
                        best_state = new_state
                        best_cost = new_cost
                        self.tabu_list.append(move)
                        if len(self.tabu_list) > self.tabu_size:
                            self.tabu_list.pop(0)
                        found_better = True
                        break
            if not found_better:
                logging.debug("No se encontraron mejores movimientos, posible óptimo local alcanzado")
                break

        return best_state
    
    
    

class StochasticLocalSearch:
    def __init__(self, get_moves, first_improvement=True):
        self.get_moves = get_moves
        self.first_improvement = first_improvement

    def __call__(self, current_solution):
        while True:
            moves = self.get_moves(current_solution)
            random.shuffle(moves)
            min_cost = current_solution.get_cost()
            best_move = None
            for move in moves:
                solution_cost = current_solution.get_cost_after_move(move)
                if solution_cost < min_cost:
                    min_cost = solution_cost
                    best_move = move
                    if self.first_improvement:
                        break
            if best_move is None:
                break
            current_solution.intentar_movimiento(best_move)
        return current_solution





class DefaultAcceptanceCriterion:
    def __call__(self, best_cost, new_cost):
        return new_cost < best_cost


class Perturbation:
    def __init__(self, ambiente, pert_size=3):
        self.ambiente = ambiente
        self.pert_size = pert_size

    def __call__(self, state):
        current_state = deepcopy(state)
        for _ in range(self.pert_size):
            moves = self.ambiente.generar_acciones(current_state, tipo="warnsdorf")
            if moves:
                move = random.choice(moves)
                current_state.actualizar_estado(move)
        return current_state

class ILS:
    def __init__(self, local_search, perturbation, max_iter=50):
        self.local_search = local_search
        self.perturbation = perturbation
        self.max_iter = max_iter
        
    def __call__(self, initial_solution):
        current_solution = deepcopy(initial_solution)
        best_solution = deepcopy(current_solution)
        best_cost = len(best_solution.visitados)

        for _ in range(self.max_iter):
            perturbed_solution = self.perturbation(current_solution)
            local_optimum = self.local_search(perturbed_solution)
            local_cost = len(local_optimum.visitados)
            
            if local_cost > best_cost:
                best_solution = deepcopy(local_optimum)
                best_cost = local_cost

        return best_solution


class AStar:
    def __init__(self, admissibleHeuristic, getConstructiveMoves):
        self.admissibleHeuristic = admissibleHeuristic
        self.getConstructiveMoves = getConstructiveMoves

    def __call__(self, initial_state):
        open_set = []
        heapq.heappush(open_set, (0, initial_state))

        while open_set:
            _, current_state = heapq.heappop(open_set)
            if current_state.isCompleteSolution():
                return current_state

            for move in self.getConstructiveMoves(current_state):
                new_state = deepcopy(current_state)
                new_state.intentar_movimiento(move)
                estimated_total_cost = new_state.get_cost() + self.admissibleHeuristic(new_state)
                heapq.heappush(open_set, (estimated_total_cost, new_state))
        return None


