import unittest
import networkx as nx
from KnightsTour import InstanciaGrafo, EstadoGrafo, AmbienteGrafo

global N

class TestInstanciaGrafo(unittest.TestCase):
    def test_crear_grafo(self):
        N = 5
        instancia = InstanciaGrafo(N)
        grafo = instancia.grafo
        # El número esperado de aristas variará; esto solo verifica que el grafo se crea con nodos
        self.assertEqual(len(grafo.nodes()), N * N)

    def test_obtener_movimientos_legales(self):
        N = 5
        instancia = InstanciaGrafo(N)
        # Verificar movimientos legales en una posición central
        self.assertIn((3, 2), instancia.obtener_movimientos_legales((4, 4)))
        # Verificar que los movimientos ilegales no estén incluidos
        self.assertNotIn((5, 5), instancia.obtener_movimientos_legales((4, 4)))

class TestEstadoGrafo(unittest.TestCase):
    def setUp(self):
        N = 5
        self.instancia = InstanciaGrafo(N)
        self.estado = EstadoGrafo(self.instancia, (0, 0))

    def test_costo(self):
        # Iniciar con un costo de 1, ya que una casilla está visitada
        self.assertEqual(self.estado.costo(), -1)

    def test_consistencia_estado(self):
            self.estado.actualizar_estado((2, 1))
            self.assertIn((2, 1), self.estado.visitados)
            self.assertEqual(self.estado.posicion_actual, (2, 1))

            # Verificar que no hay duplicados en visitados y que los movimientos ilegales se manejan correctamente
            with self.assertRaises(ValueError):
                self.estado.actualizar_estado((2, 1))  # Intento de mover a una posición ya visitada

    def test_actualizar_estado(self):
        # Asegurarse de que la actualización funcione correctamente para movimientos legales
        self.assertTrue(self.estado.actualizar_estado((2, 1)))
        self.assertIn((2, 1), self.estado.visitados)


if __name__ == '__main__':
    unittest.main()