# KnightsTour 
from KnightsTour import (
    InstanciaGrafo, EstadoGrafo, AmbienteGrafo, evalWarnsdorfActions,
    evalConstructiveActions, local_search_method, plot_knights_tour
)

# Classic algorithms 
from classic_algorithms import *

# Agents 
from agents import (
    AgenteWarnsdorfBusquedaLocal, Perturbation, ILS, local_search_method,
    TabuSearchAgent, KnightTourDFS, KnightTourBFS
)

# Standard library imports
import json
import os
import random
import time

# NumPy and matplotlib imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# TensorFlow and Keras imports
import tensorflow as tf
from tensorflow.keras.layers import ( # type: ignore
    Input, Dense, Lambda, Softmax, Layer, Flatten, TimeDistributed, Add,
    LayerNormalization, Dropout, MultiHeadAttention
)
from tensorflow.keras import backend as K

import keras.utils
from tensorflow.keras.layers import Reshape

from tensorflow.keras.models import Model, Sequential # type: ignore
from tensorflow.keras.utils import to_categorical # type: ignore

# Main imports
from main import run_algorithm, test_knights_tour, save_results

from tensorflow.keras.models import load_model # type: ignore


""" 
 .d888888                               dP            
d8'    88                               88            
88aaaaa88a .d8888b. .d8888b. 88d888b. d8888P .d8888b. 
88     88  88'  `88 88ooood8 88'  `88   88   88ooood8 
88     88  88.  .88 88.  ... 88    88   88   88.  ... 
88     88  `8888P88 `88888P' dP    dP   dP   `88888P' 
                .88                                   
            d8888P                                    
"""

class TabuSearchAgent:
    def __init__(self, environment, start_position, tabu_size=10, max_iterations=1000):
        self.environment = environment
        self.current_position = start_position
        self.tabu_list = []
        self.tabu_size = tabu_size
        self.max_iterations = max_iterations
        self.estado = EstadoGrafo(environment.instancia_grafo, start_position)
        self.iteracion_costo = []

    def search(self):
        self.iteracion_costo = []
        for iter_num in range(self.max_iterations):
            if self.estado.es_completo:
                # Agrega la última iteración y costo antes de retornar
                self.iteracion_costo.append((iter_num, self.estado.nuevo_costo()))
                # Asegúrate de retornar ambos valores aquí
                return self.estado.visitados, self.iteracion_costo

            possible_actions = self.environment.generar_acciones(self.estado)
            if not possible_actions:
                break
            best_action = self.select_next_position(possible_actions)
            self.estado.actualizar_estado(best_action)
            # Guardamos la iteración y el nuevo costo
            self.iteracion_costo.append((iter_num, self.estado.nuevo_costo()))

        # También retorna ambos valores aquí para mantener la consistencia
        return self.estado.visitados if self.estado.es_completo else None, self.iteracion_costo
        # También retorna ambos valores aquí para mantener la consistencia
        return self.estado.visitados
    
    def select_next_position(self, neighbors):
        best_position = None
        min_unvisited_neighbors = float('inf')
        for position in neighbors:
            if position not in self.estado.visitados:
                possible_moves = self.environment.instancia_grafo.obtener_movimientos_legales(position)
                unvisited_neighbors_count = len([move for move in possible_moves if move not in self.estado.visitados])
                if unvisited_neighbors_count < min_unvisited_neighbors:
                    min_unvisited_neighbors = unvisited_neighbors_count
                    best_position = position
        return best_position

"""# **Crear problema y datos de entrenamiento**"""




""" 
 88888888b                            oo                                     
 88                                                                          
a88aaaa    dP    dP 88d888b. .d8888b. dP .d8888b. 88d888b. .d8888b. .d8888b. 
 88        88    88 88'  `88 88'  `"" 88 88'  `88 88'  `88 88ooood8 Y8ooooo. 
 88        88.  .88 88    88 88.  ... 88 88.  .88 88    88 88.  ...       88 
 dP        `88888P' dP    dP `88888P' dP `88888P' dP    dP `88888P' `88888P' 
                                                                             
                                dP                     oo          
                                88                                 
dP.  .dP          dP    dP    d8888P 88d888b. .d8888b. dP 88d888b. 
 `8bd8'           88    88      88   88'  `88 88'  `88 88 88'  `88 
 .d88b.     dP    88.  .88      88   88       88.  .88 88 88    88 
dP'  `dP    88    `8888P88      dP   dP       `88888P8 dP dP    dP 
            .P         .88                                         
                   d8888P       
                   
                                                                                                                                                                                                                                                
"""

def crear_problemas(N):
  start_position = (random.randint(0, N-1), random.randint(0, N-1))
  graph_instance = InstanciaGrafo(N)
  estado_inicial = EstadoGrafo(graph_instance, start_position)
  environment = AmbienteGrafo(graph_instance)

  return environment, start_position


def aplicar_tabusearch(environment, start_position=None, tabu_size=10, max_iterations=1000):
  if start_position is None:
    start_position = (random.randint(0, environment.instancia_grafo.N-1), random.randint(0, environment.instancia_grafo.N-1))
    
  tabu_search_agent = TabuSearchAgent(environment, start_position, tabu_size, max_iterations)
  # verificar si se encuentra solucion
  ruta, iteracion_costo = tabu_search_agent.search()
  return ruta, iteracion_costo


def crear_datos_entrenamiento(N, num_muestras):
    X, Y = [], []
    for _ in range(num_muestras):
        environment, start_position = crear_problemas(N)
        ruta, iteracion_costo = aplicar_tabusearch(environment, start_position)
        # Asegurarse de que la ruta es completa verificando si tiene N*N elementos
        if ruta and len(ruta) == N * N:
            for i in range(len(ruta) - 1):
                tablero = [['0']*N for _ in range(N)]
                historial_movimientos = ruta[:i+1]  # Incluyendo la posición actual en el historial de movimientos
                # Marcar las casillas visitadas con '1'
                for pos in historial_movimientos:
                    tablero[pos[0]][pos[1]] = '1'
                tablero[ruta[i][0]][ruta[i][1]] = 'C'  # Marcar la posición actual del caballo con 'C'
                if i > 0:
                    movimientos_diferencia = [(historial_movimientos[j][0] - historial_movimientos[j-1][0],
                                               historial_movimientos[j][1] - historial_movimientos[j-1][1]) for j in range(1, i+1)]
                else:
                    movimientos_diferencia = []
                # Marcar los posibles movimientos con 'X', solo si no están visitados
                movimientos_legales = environment.instancia_grafo.obtener_movimientos_legales(ruta[i])
                for movimiento in movimientos_legales:
                    if tablero[movimiento[0]][movimiento[1]] == '0':  # Solo marcar con 'X' si no ha sido visitado
                        tablero[movimiento[0]][movimiento[1]] = 'X'

                X.append((tablero, movimientos_diferencia))  # Agregar el estado actual del tablero y el historial de movimientos

                siguiente_movimiento = (ruta[i+1][0] - ruta[i][0], ruta[i+1][1] - ruta[i][1])
                Y.append(siguiente_movimiento)  # Agregar el siguiente movimiento esperado
    return X, Y


def transform_input(X_train_np):
    stack = []
    for i in range(X_train_np.shape[0]):
        vector = X_train_np[i].flatten()
        horse_plane = (X_train_np[i] == 'C').astype(int)
        visited_plane = ((X_train_np[i] == '1') | (X_train_np[i] == 'C')).astype(int)
        movable_plane = (X_train_np[i] == 'X').astype(int)
        # Concatenar planos
        input_stack = np.dstack((horse_plane, visited_plane, movable_plane))
        stack.append(input_stack)
    return np.array(stack)


def convert_to_numpy_arrays(X, Y):
    X_np = np.array([x[0] for x in X])
    Y_np = np.array(Y)
    return X_np, Y_np


def transform_movements_to_categorical(Y):
    move_to_index = {
        (-2, -1): 0, (-1, -2): 1, (1, -2): 2, (2, -1): 3,
        (2, 1): 4, (1, 2): 5, (-1, 2): 6, (-2, 1): 7
    }
    Y_indices = np.array([move_to_index[(dy, dx)] for dy, dx in Y])
    Y_categorical = to_categorical(Y_indices, num_classes=8)
    return Y_categorical



""" 


dP                                           
88                                           
88  .dP  .d8888b. 88d888b. .d8888b. .d8888b. 
88888"   88ooood8 88'  `88 88'  `88 Y8ooooo. 
88  `8b. 88.  ... 88       88.  .88       88 
dP   `YP `88888P' dP       `88888P8 `88888P' 
                                             
                                             
                                             """


"""# **Modelo Keras**

La red neuronal consta de varias capas:

  1. La capa de entrada recibe el tablero de ajedrez como una matriz de 8x8x3.
  2. La capa de reshape transforma la entrada en una matriz de 64x3.
  3. La capa de atención multi-cabeza procesa la entrada y produce 4.una salida que se centra en las características más relevantes del tablero.
  4. La capa de flatten transforma la salida de la capa de atención en una matriz unidimensional.
  5. La capa densa 1 y la capa densa 2 realizan predicciones sobre el próximo movimiento del caballo.

"""


def inverse_transform_categorical_to_movements(Y_categorical):
    index_to_move = {
        0: (-2, -1), 1: (-1, -2), 2: (1, -2), 3: (2, -1),
        4: (2, 1), 5: (1, 2), 6: (-1, 2), 7: (-2, 1)
    }
    Y_indices = np.argmax(Y_categorical, axis=1)
    Y_probs = np.max(Y_categorical, axis=1)
    Y_moves = {index_to_move[idx]: prob for idx, prob in zip(Y_indices, Y_probs)}
    return Y_moves

# Función para mostrar el movimiento predicho con su probabilidad
def mostrar_movimiento_predicho(movimiento_predicho):
    for movimiento, probabilidad in movimiento_predicho.items():
        print(f"Posible movimiento: {movimiento}, Probabilidad: {probabilidad:.4f}")

### 

def get_state_representation(instance, state, environment):
    # Crear una matriz 8x8x3 para representar el estado actual del tablero
    board = np.zeros((8, 8, 3), dtype=int)

    # Marcar las casillas visitadas con '1'
    for pos in state.visitados:
        board[pos[0], pos[1], 0] = 1

    # Marcar la posición actual del caballo con 'C'
    board[state.current_position[0], state.current_position[1], 0] = 2

    # Marcar los posibles movimientos con 'X', solo si no están visitados
    possible_moves = environment.instancia_grafo.obtener_movimientos_legales(state.current_position)
    for move in possible_moves:
        if move not in state.visitados:
            board[move[0], move[1], 0] = 3

    return board

def get_possible_moves(model_output):
    possible_moves = []
    for i, prob in enumerate(model_output[0]):
        move = index_to_move[i]
        possible_moves.append((move, prob))
    return possible_moves


def predict_next_move(instance, state, environment):
    board = get_state_representation(instance, state, environment)
    board = np.expand_dims(board, axis=0)  # Añadir una dimensión para que coincida con la forma esperada por el modelo
    model_output = model.predict(board)
    possible_moves = get_possible_moves(model_output)
    return possible_moves

environment, start_position =crear_problemas(8)

def crear_problemas(N):
  start_position = (random.randint(0, N-1), random.randint(0, N-1))
  graph_instance = InstanciaGrafo(N)
  estado_inicial = EstadoGrafo(graph_instance, start_position)
  environment = AmbienteGrafo(graph_instance)

  return graph_instance,estado_inicial,environment,start_position

def visualize_planes(planes):
    fig, axs = plt.subplots(1, 3, figsize=(12, 4))
    axs[0].imshow(planes[:, :, 0], cmap='gray')
    axs[1].imshow(planes[:, :, 1], cmap='gray')
    axs[2].imshow(planes[:, :, 2], cmap='gray')
    plt.show()


def compute_output_shape(self, input_shape):
    shape = list(input_shape)
    assert len(shape) == 3  # only valid for 3D tensors
    shape[-1] = self.output_dim
    return tuple(shape)


@keras.utils.register_keras_serializable()
class CustomMaskLayer(Layer):
    def call(self, inputs):
        # Generar máscara: 1 si el vector no es completamente ceros, 0 si lo es
        mask = tf.reduce_any(inputs!= 0, axis=-1)
        mask = tf.expand_dims(tf.expand_dims(mask, 1), 1)
        return mask
    
