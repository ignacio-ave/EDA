
# Knight's Tour Problem (Problema del Tour del Caballo)

# Definición:
# El Problema del Tour del Caballo es un desafío matemático que consiste en encontrar una ruta por la cual
# un caballo de ajedrez pueda visitar cada casilla de un tablero de ajedrez vacío exactamente una vez. 
# El caballo debe seguir las reglas estándar de movimiento en ajedrez, moviéndose en forma de 'L' (dos casillas
# en una dirección y una en la perpendicular).

# Formulación del Problema:
# Dado un tablero de ajedrez de dimensiones N x N y una casilla inicial, se busca una secuencia de movimientos
# del caballo que permita visitar todas las casillas del tablero una única vez.

# Entrada:
# - Un entero N que representa las dimensiones del tablero de ajedrez (N x N).
# - Una casilla inicial (x0, y0) donde el caballo comienza su recorrido.
# - Casillas a recorrer (opcional): Se puede especificar un subconjunto de casillas que deben ser visitadas. 
#   Si no se especifica, el objetivo es visitar todas las casillas del tablero.

# Salida:
# - Una secuencia de movimientos del caballo que permita visitar todas las casillas del tablero una única vez.
#   Opcionalmente, si se le pasa casillas a recorrer, la secuencia debe ser una en la que se minimicen los movimientos.
# - Un valor booleano que indica si se ha encontrado una solución o no.

# Ejemplo:

# Entrada: N = 5, (0, 0)

# Salida:   ([(0, 0), (2, 1), ... , (2, 2), (0, 4)] , True)



# Función Objetivo:
# Encontrar una secuencia de movimientos del caballo tal que cada casilla del tablero sea visitada exactamente una vez.
# Se puede considerar una función de costo que mida la cantidad de casillas visitadas o la eficiencia de la secuencia.
# El objetivo es minimizar la cantidad de movimientos y maximizar los puntos a visitar.



# Restricciones:
# - Cada movimiento debe ser legal según las reglas de ajedrez para movimientos del caballo, es decir, dos casillas en una
#   dirección y una en la perpendicular.
# - No se puede visitar una casilla más de una vez durante el recorrido.
#   Si esto ocurre, debemos retornar la secuencia de movimientos que maximice la cantidad de casillas visitadas y minimice los movimientos.



import numpy as np
import networkx as nx
import random
from copy import deepcopy


# Clase InstanciaGrafo
# Representa el tablero de ajedrez como un grafo donde cada casilla es un nodo y cada movimiento legal del caballo es una arista.
#
# Atributos:
# - N: El tamaño del tablero (N x N).
# - grafo: Un objeto Graph de la librería networkx que representa el tablero y los movimientos del caballo.
#
# Métodos:
# - __init__(self, N): Constructor que inicializa el grafo con el tamaño de tablero especificado.
# - _crear_grafo(self): Método privado para crear los nodos y aristas del grafo basados en los movimientos del caballo.
# - es_movimiento_legal(self, origen, destino): Comprueba si existe una arista entre los nodos origen y destino, lo que 
#   indicaría un movimiento legal del caballo.
# - obtener_movimientos_legales(self, posicion): Retorna una lista de posiciones legales a las que el caballo puede moverse 
#   desde la posición actual.


class InstanciaGrafo:
    def __init__(self, N):
        self.N = N
        self.grafo = nx.Graph()
        self._crear_grafo()

    def _crear_grafo(self):
        # Crear nodos para cada posición en el tablero de ajedrez
        for x in range(self.N):
            for y in range(self.N):
                self.grafo.add_node((x, y))

        # Añadir aristas para movimientos legales del caballo
        movimientos_caballo = [(2, 1), (1, 2), (-1, 2), (-2, 1),
                               (-2, -1), (-1, -2), (1, -2), (2, -1)]

        for x in range(self.N):
            for y in range(self.N):
                for dx, dy in movimientos_caballo:
                    nueva_x, nueva_y = x + dx, y + dy
                    if (0 <= nueva_x < self.N) and (0 <= nueva_y < self.N):
                        self.grafo.add_edge((x, y), (nueva_x, nueva_y))

    def es_movimiento_legal(self, origen, destino):
        return self.grafo.has_edge(origen, destino)

    def obtener_movimientos_legales(self, posicion):
        if posicion is None:
            raise ValueError("Invalid position: None")
        return list(self.grafo.neighbors(posicion))

    def __str__(self):
        return f"InstanciaGrafo: N={self.N}, Nodos={len(self.grafo.nodes)}, Aristas={len(self.grafo.edges)}"
    


# Clase EstadoGrafo
# Gestiona el estado actual dentro del problema del Tour del Caballo, manteniendo un registro
# de las casillas visitadas y controlando la progresión hacia la solución del problema.
#
# Atributos:
# - instancia_grafo: Una referencia a la instancia del grafo que representa el tablero de ajedrez.
# - posicion_actual: La casilla actual del tablero en la que se encuentra el caballo.
# - visitados: Un conjunto de casillas que ya han sido visitadas por el caballo.
# - es_completo: Un booleano que indica si se ha alcanzado una solución completa, es decir,
#   si el caballo ha visitado todas las casillas.
#
# Métodos:
# - costo(): Devuelve el número de movimientos realizados hasta el momento.
# - actualizar_estado(nueva_posicion): Actualiza la posición actual del caballo y el conjunto de visitados.
#   Si se intenta un movimiento ilegal, se levanta una excepción.

class EstadoGrafo:
    def __init__(self, instancia_grafo, posicion_inicial, objetivos_visitar=None):
        self.instancia_grafo = instancia_grafo
        self.posicion_actual = posicion_inicial
        self.objetivos_visitar = objetivos_visitar if objetivos_visitar else list(instancia_grafo.grafo.nodes())
        self.visitados = [posicion_inicial]  # Cambiado de set a lista
        self.es_completo = False
        self.fallos_consecutivos = 0 


    def costo(self):
        return len(self.visitados)
    
    def nuevo_costo(self):
        return self.instancia_grafo.N ** 2 - len(self.visitados)
    
    def actualizar_estado(self, nueva_posicion):
        if nueva_posicion not in self.visitados:
            self.posicion_actual = nueva_posicion
            self.visitados.append(nueva_posicion)
            self.es_completo = len(self.visitados) == len(self.instancia_grafo.grafo.nodes())
            return True
        return False

    def intentar_movimiento(self, nueva_posicion):
        if self.instancia_grafo.es_movimiento_legal(self.posicion_actual, nueva_posicion) and nueva_posicion not in self.visitados:
            if self.actualizar_estado(nueva_posicion):
                return self  # Retorna el estado actualizado
        return None  # Retorna None si el movimiento no es legal o ya fue visitado

    
    def revertir_estado(self):
        if len(self.visitados) > 1:
            self.visitados.remove(self.posicion_actual)
            self.posicion_actual = list(self.visitados)[-1]
        else:
            raise ValueError("No se puede revertir el estado a la posición inicial")
    
    def isCompleteSolution(self):
        return self.es_completo
    
    def reiniciar_estado(self):
        start_position = (random.randint(0, self.instancia_grafo.N-1), random.randint(0,  self.instancia_grafo.N-1))
        self.posicion_actual = start_position
        self.visitados = [self.posicion_actual]
        self.es_completo = False
        self.fallos_consecutivos = 0
    

        

    def __str__(self):
        return f"EstadoGrafo: Posición Actual={self.posicion_actual}, Visitados={self.visitados}, Completo={self.es_completo}"
    
    
    
# Clase AmbienteGrafo
# Proporciona el entorno en el que se desarrolla el Tour del Caballo, encargándose de generar
# las acciones posibles y manejar las transiciones de estado según las reglas del problema.
#
# Métodos:
# - generar_acciones(estado): Consulta la instancia del grafo para obtener los movimientos legales desde
#   la posición actual del caballo. Si los movimientos ya están en caché, los devuelve directamente para
#   evitar cálculos innecesarios.

class AmbienteGrafo:
    def __init__(self, instancia_grafo):
        self.cache_movimientos_legales = {}
        self.instancia_grafo = instancia_grafo
        
    def generar_acciones(self, estado, tipo="normal"):
        posicion_actual = estado.posicion_actual

        # Handle the case where the current position is None
        if posicion_actual is None:
            # If there are no visited positions or the last visited is None, reset the state
            if not estado.visitados or estado.visitados[-1] is None:
                estado.reiniciar_estado()
            else:
                # Resume from the last valid position
                estado.posicion_actual = estado.visitados[-1]

            return self.instancia_grafo.obtener_movimientos_legales(estado.posicion_actual)

        # If the tour is complete, return an empty list to indicate no further actions are possible
        if len(estado.visitados) == len(self.instancia_grafo.grafo.nodes):
            return []


        if tipo == "normal":
            
            if posicion_actual not in self.cache_movimientos_legales:
                movimientos_legales = estado.instancia_grafo.obtener_movimientos_legales(posicion_actual)
                self.cache_movimientos_legales[posicion_actual] = movimientos_legales
            return self.cache_movimientos_legales[posicion_actual]

        elif tipo == "warnsdorf":
            return self.evalWarnsdorfActions(estado)

        elif tipo == "dfs":
            # Asumiendo que el DFS simplemente necesitará todos los movimientos legales desde la posición actual.
            return self.generar_acciones(estado, "normal")

        elif tipo == "bfs":
            # Para BFS, simplemente retornamos los movimientos legales como en el modo normal.
            return self.generar_acciones(estado, "normal")

        elif tipo == "random":
            moves = self.generar_acciones(estado, "normal")
            random.shuffle(moves)
            return moves

    def evalWarnsdorfActions(self, estado):
        actions = self.generar_acciones(estado, "normal")
        evaluated_actions = []
        for action in actions:
            if action not in estado.visitados:
                temp_state = deepcopy(estado)
                temp_state.actualizar_estado(action)
                next_moves = self.generar_acciones(temp_state, "normal")
                score = len(next_moves)
                evaluated_actions.append((action, score))
        evaluated_actions.sort(key=lambda x: x[1])
        return [action for action, _ in evaluated_actions]



    def calcular_costo_movimiento(self, estado, posicion_nueva):
        """Calcula el costo de moverse a una nueva posición sin actualizar el estado."""
        return len(estado.visitados) + 1 if posicion_nueva not in estado.visitados else np.inf

    def transicion_estado(self, estado, nueva_posicion):
        try:
            estado.actualizar_estado(nueva_posicion)
            return True
        except ValueError as e:
            print("No se encontró una solución.")
            return False
        
    def retroceder_estado(self, estado):
        try:
            estado.revertir_estado()
            return True
        except ValueError as e:
            print("No se puede retroceder más.")
            return False
    
    def __str__ (self):
        return "AmbienteGrafo"
        



    
    
def evalConstructiveActions(state, env):
    evals = []
    for move in env.generar_acciones(state):
        if move not in state.visitados:
            temp_state = deepcopy(state)
            temp_state.actualizar_estado(move)
            score = 1 / len(env.generar_acciones(temp_state)) if len(env.generar_acciones(temp_state)) > 0 else float('inf')
            evals.append((move, score))
            
    return evals

def evalWarnsdorfActions(state, env):
    actions = env.generar_acciones(state)
    evaluated_actions = []
    for action in actions:
        if action not in state.visitados:
            temp_state = deepcopy(state)
            temp_state.actualizar_estado(action)
            next_moves = env.generar_acciones(temp_state)
            score = len(next_moves)
            evaluated_actions.append((action, score))
    # Ordenar los movimientos por el número de opciones sucesoras (ascendente)
    evaluated_actions.sort(key=lambda x: x[1])
    return evaluated_actions




#################
import numpy as np
from agents import *
from classic_algorithms import *
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.style.use('ggplot')



# Función para graficar la solución del tour del caballo en el notebook
def plot_knights_tour(moves, board_size):
    fig, ax = plt.subplots(figsize=(board_size, board_size))
    ax.set_xlim(-0.5, board_size - 0.5)
    ax.set_ylim(-0.5, board_size - 0.5)
    edge_color = '#2F343B'

    # Dibujar los nodos y las líneas de la trayectoria
    for i, move in enumerate(moves):
        if i == 0:  # Nodo inicial
            node_style = dict(radius=0.15, facecolor='#7E827A', edgecolor=edge_color, zorder=4)
            ax.add_patch(patches.Circle((move[0], board_size - move[1] - 1), **node_style))
        else:  # Nodos restantes
            node_style = dict(radius=0.1, facecolor='#7E827A', edgecolor=edge_color, zorder=4)
            ax.add_patch(patches.Circle((move[0], board_size - move[1] - 1), **node_style))

        # Línea hasta el próximo nodo
        if i < len(moves) - 1:
            next_move = moves[i + 1]
            line_style = dict(color='#703030', linewidth=2, zorder=3)
            ax.plot([move[0], next_move[0]], [board_size - move[1] - 1, board_size - next_move[1] - 1], **line_style)

    # Remover los ejes y mostrar la gráfica
    ax.axis('off')
    plt.show()
    

